FROM node:12-alpine

ENV REDIS_VERSION 5.0.5
ENV REDIS_DOWNLOAD_URL http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz

RUN apk update && apk upgrade \
    && apk add --update --no-cache --virtual build-deps gcc make linux-headers musl-dev tar \
    && wget -O redis.tar.gz "$REDIS_DOWNLOAD_URL" \
    && mkdir -p /usr/src/redis \
    && tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1 \
    && rm redis.tar.gz \
    && make -C /usr/src/redis install redis-cli /usr/bin \
    && rm -r /usr/src/redis \
    && apk del build-deps \
    && rm -rf /var/cache/apk/*


ENTRYPOINT []
CMD ["node", "redis-cluster"]

EXPOSE 3000

RUN mkdir -p /srv
WORKDIR /srv
RUN mkdir -p /tmp/srv
COPY srv/package.json srv/package-lock.json /tmp/srv/
RUN cd /tmp/srv && npm install
RUN cp -a /tmp/srv/* /srv/

COPY srv/ /srv/

COPY bin/ /usr/local/bin/
